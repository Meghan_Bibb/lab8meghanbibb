Meghan Bibb
Lab 8

1. 
The Timer class choses to only print INFO errors to the console. 
The code prints all errors to the Logger.log.
This is controlled by the logger.properties file which specifies the preferences
of the logger and how it should print/ignore logs. 

Console Output:
2019-03-25 14:55:54 INFO edu.baylor.ecs.si.Timer <clinit> starting the app
2019-03-25 14:55:54 INFO edu.baylor.ecs.si.Timer timeMe Calling took: 0
2019-03-25 14:55:54 INFO edu.baylor.ecs.si.Timer timeMe * should take: 0
2019-03-25 14:55:55 INFO edu.baylor.ecs.si.Timer timeMe Calling took: 1003
2019-03-25 14:55:55 INFO edu.baylor.ecs.si.Timer timeMe * should take: 1000

Loger.log Output:
2019-03-25 14:55:54 INFO edu.baylor.ecs.si.Timer <clinit> starting the app
2019-03-25 14:55:54 INFO edu.baylor.ecs.si.Timer timeMe Calling took: 0
2019-03-25 14:55:54 INFO edu.baylor.ecs.si.Timer timeMe * should take: 0
2019-03-25 14:55:54 FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
2019-03-25 14:55:54 FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
2019-03-25 14:55:55 INFO edu.baylor.ecs.si.Timer timeMe Calling took: 1003
2019-03-25 14:55:55 INFO edu.baylor.ecs.si.Timer timeMe * should take: 1000

2. This line comes from the logger within JUnit. Specificically, this message is coming from the ConditionEvaluator.
3.What does Assertions.assertThrows do?
	The assertThrows asserts that a given code segment runs without throwing an exception.
	If the code throws an exception, the assertThrows throws an exception.
	This is often used alongside assertEquals.

4. See TimerException and there are 3 questions
	What is serialVersionUID and why do we need it? (please read on Internet)
		We need serialVersionUID because it provides support for backwards
		and forwards compatibility.  It gives every serializable class a version number
		which can be utilized during deserialization to know the reciever and sender
 		of a object that has been serialized.  It must be static, final, and a long.

Why do we need to override constructors?
		We need to override constructors to be able to construct a TimerException object, instead of constructing
		a Exception object when no TimerException constructors are written. This allows us to make a TimerException object.
	
Why we did not override other Exception methods?
		We did not override them because we did not need to change them.  We can use the methods 
		as they are written with the TimerException class.

5. The Timer.java has a static block static {}, what does it do? (determine when called by debugger)
	This block only runs once, with the creation of the first Timer object.  
	It opens the configuration file, reads it and then closes it.  It reads in from the file "logger.properties".
	It prints "2019-03-25 14:55:54 INFO edu.baylor.ecs.si.Timer <clinit> starting the app"

6. The format of the README.md is done in Markdown syntax.  The Bitbucket Server also uses Markdown for formatting text.

7. Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)
	The test failed because timeNow was never initialized in the test where -1 is passed as a parameter.
	To fix this I moved the initialization of timeNow to before the throw statement.
	
8. What is the actual issue here, what is the sequence of Exceptions and handlers (debug)?
	The actual issue is that TimerException is not dealt with or caught in the method timeMe.  
	TimerException is thrown in the try block of timeMe and effectively swallowed by the method, which should not be the case. It is not handled, just passed out of the method. 
	
11. What category of Exceptions is TimerException and what is NullPointerException?
	NullPointerException is a RuntimeException which is unchecked.
	TimerException is a checked exception.
	


